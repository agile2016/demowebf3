﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="DemoWebF3.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="https://code.jquery.com/jquery-3.1.0.min.js"></script>
    <script src="http://cdn.alloyui.com/3.0.1/aui/aui-min.js"></script>
    <link href="http://cdn.alloyui.com/3.0.1/aui-css/css/bootstrap.min.css" rel="stylesheet">
    <script>
        var insertEvent, loadData;
    </script>
</head>
<body>
    <div class="container">
        <form id="myForm" runat="server">

            <div class="form-group">
                <label class="control-label" for="iName">Name:</label>
                <div class="controls">
                    <input name="iName" id="iName" class="form-control field-required" type="text" runat="server">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label" for="age">Age:</label>
                <div class="controls">
                    <input name="age" id="age" class="form-control field-required field-digits" type="text">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label" for="email">E-mail:</label>
                <div class="controls">
                    <input name="iEmail" id="iEmail" class="form-control field-required field-email" type="text" runat="server">
                </div>
            </div>

            <asp:Button class="btn btn-info" type="submit" runat="server" ID="b1" CssClass="btn btn-info" Text="Submit" />
            <input class="btn btn-primary" type="reset" value="Reset">
            <input class="btn btn-primary" type="button" value="Ajax post" onclick="insertEvent()">
        </form>
    </div>

    <div id="myDataTable"></div>

    <script runat="server">        
        protected String GetTime()
        {
            return DateTime.Now.ToString("t");
        }

        public String abc = "hello";



    </script>
    <h2><% =abc %></h2>
    <script>
        YUI().use('aui-form-validator', function (Y) {
            new Y.FormValidator(
              {
                  boundingBox: '#myForm'
              }
            );
        });
        YUI().use(
  'aui-datatable',
  'aui-datatype',
  'datatable-sort',
  function (Y) {
      var data = [];

      <%for (int i = 0; i < myDT.Rows.Count; i++)
        {
            var r = myDT.Rows[i];%>
      data.push({ name: "<%=r.ItemArray[1]%>" });
        <%}%>


      console.log(data);

      var nameEditor = new Y.TextAreaCellEditor(
        {
            validator: {
                rules: {
                    name: {
                        email: true,
                        required: true
                    }
                }
            }
        }
      );

      var nestedCols = [
        {
            editor: new Y.TextCellEditor,
            key: 'Name',
            sortable: true
        },
        {
            editor: nameEditor,
            key: 'Email',
            sortable: true
        }
      ];

      var createTable = function (tempData) {
          $('#myDataTable').html('');
          var dataTable = new Y.DataTable({
              columns: nestedCols,
              data: tempData,//remoteData,
              editEvent: 'dblclick',
              plugins: [
                {
                    cfg: {
                        highlightRange: false
                    },
                    fn: Y.Plugin.DataTableHighlight
                }
              ]
          }).render('#myDataTable');

          dataTable.get('boundingBox').unselectable();
      };


      loadData = function() {
          $.ajax({
              method: "POST",
              url: "/WebService1.asmx/LoadData"
          }).done(function (msg) {
              //alert("Data Saved: " + msg);
              //console.log(msg);
              data = JSON.parse(msg.firstChild.firstChild.data);
              console.log(data);

              createTable(data);
          });
      }

      loadData();

  }
);


    </script>
    <script>
        insertEvent = function () {
            $.ajax({
                method: "POST",
                url: "/WebService1.asmx/InsertData", contentType: 'application/x-www-form-urlencoded',
                data: 'name=' + $('#iName').val() + '&email=' + $('#iEmail').val()
            }).done(function (msg) {
                //alert("Data Saved: " + msg);
                //console.log(msg);
                data = JSON.parse(msg.firstChild.firstChild.data);
                console.log(data);

                loadData();
            });
        };

    </script>
</body>
</html>
