﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Newtonsoft;
using System.Data.SqlClient;
using System.Data;

namespace DemoWebF3
{
    /// <summary>
    /// Summary description for WebService1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WebService1 : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        public string LoadData()
        {
            var result = "";
            var connectionString = @"Data Source=.\sql2008;Initial Catalog=DemoWebF3;Integrated Security=True;Pooling=False";
            var connection = new SqlConnection(connectionString);
            var cmdText = "select * from product";
            //var cm = new SqlCommand(cmdText, connection);
            var adt = new SqlDataAdapter(cmdText, connectionString);
            var dt = new DataTable();
            adt.Fill(dt);

            result = Newtonsoft.Json.JsonConvert.SerializeObject(dt);

            return result;
        }

        [WebMethod]
        public string InsertData(string name, string email)
        {
            var result = "";
            var connectionString = @"Data Source=.\sql2008;Initial Catalog=DemoWebF3;Integrated Security=True;Pooling=False";
            var connection = new SqlConnection(connectionString);
            var cmdText = "INSERT INTO dbo.Product( Name, Email )VALUES  ( @Name,@Email);SELECT SCOPE_IDENTITY();";
            var cm = new SqlCommand(cmdText, connection);

            cm.Parameters.Add(new SqlParameter { ParameterName = "@Name", DbType = DbType.String, Value = name });
            cm.Parameters.Add(new SqlParameter { ParameterName = "@Email", DbType = DbType.String, Value = email });

            var adt = new SqlDataAdapter(cm);
            var dt = new DataTable();
            adt.Fill(dt);

            var newId = dt.Rows[0].ItemArray[0];

            result = Newtonsoft.Json.JsonConvert.SerializeObject(newId);

            return result;


        }
    }
}
